#
# helper.py
#
# dot-net-bot - A Discord bot to help confused C# programmers
# Copyright (c) 2017 Ammon Smith
#
# dot-net-bot is available free of charge under the terms of the MIT
# License. You are free to redistribute and/or modify it under those
# terms. It is distributed in the hopes that it will be useful, but
# WITHOUT ANY WARRANTY. See the LICENSE file for more details.
#

from command import CommandExecutor
from state import State
from timeout import Timeout
from util import time_string
import asyncio
import re
import time

__all__ = [
    'BotHelper',
]

class BotHelper(object):
    def __init__(self, config, logger):
        self.logger = logger
        self.channels = set(config['listen-channels'])
        self.message = config['message']
        self.dest_channels = config['correct-channels']
        self.state = State(config['admins'], config['test-servers'], logger)
        self.timeout = Timeout(self.state, logger)
        self.triggers = [re.compile(r, re.DOTALL | re.IGNORECASE) for r in config['trigger-regexes']]
        self.command_exec = CommandExecutor(self.state, self.logger)

    def matches(self, string):
        for trigger in self.triggers:
            match = trigger.search(string)
            if match:
                return match
        return None

    def format(self, fmt):
        return self.message % fmt

    def get_correct_channel_mention(self, server):
        for dest_channel in self.dest_channels:
            cid = dest_channel.get('id', None)
            name = dest_channel.get('name', None)
            if cid:
                self.logger.debug("Looking for the correct channel mention for id '{}'".format(cid))
                for channel in server.channels:
                    self.logger.debug("- {} ({})".format(channel.name, channel.id))
                    if channel.id == cid:
                        self.logger.debug("Found correct channel mention: '{}'".format(channel.mention))
                        return channel.mention
            elif name:
                self.logger.debug("Looking for the correct channel mention for '#{}'".format(name))
                for channel in server.channels:
                    self.logger.debug("- {}".format(channel.name))
                    if channel.name == name:
                        self.logger.debug("Found correct channel mention: '{}'".format(channel.mention))
                        return channel.mention

        fallback = '#' + name
        self.logger.debug("Instead returning fallback channel mention: '{}'".format(fallback))
        return fallback

    @asyncio.coroutine
    def run_command(self, bot, message):
        mention = message.server.me.mention
        if mention not in message.content:
            return False

        filtered = re.sub(re.escape(mention), '', message.content)
        parts = list(filter(bool, filtered.strip().split(' ')))
        self.logger.info("Received command: {!r}".format(parts))
        if parts:
            yield from self.command_exec.run(parts, bot, message)

        return bool(parts)

