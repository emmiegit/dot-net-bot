#
# util.py
#
# dot-net-bot - A Discord bot to manage your status.
# Copyright (c) 2016 Ammon Smith and Bradley Cai
#
# dot-net-bot is available free of charge under the terms of the MIT
# License. You are free to redistribute and/or modify it under those
# terms. It is distributed in the hopes that it will be useful, but
# WITHOUT ANY WARRANTY. See the LICENSE file for more details.
#

import json

__all__ = [
    'get_client_secret',
    'get_server_id',
    'get_user_agent',
    'plural',
    'sanitize',
    'time_string',
    'PROGRAM_NAME',
    'URL',
    'VERSION',
]

PROGRAM_NAME = "dot-net-bot"
VERSION = "0.5"
URL = "https://gitlab.com/ammongit/dot-net-bot"

def get_client_secret(filename):
    with open(filename, 'r') as fh:
        return json.load(fh)

def get_server_id(server):
    if server is None:
        return '(null)'
    else:
        return server.id

def plural(count):
    return '' if count == 1 else 's'

def sanitize(msg, allow_everyone=False, allow_here=False):
    if not allow_everyone:
        msg = msg.replace('@everyone', '{everyone}')
    if not allow_here:
        msg = msg.replace('@here', '{here}')
    return msg

def get_user_agent():
    return "User-Agent: dot-net-bot_DiscordBot ({}, {})".format(URL, VERSION)

time_intervals = (
    ('year', 31536000),
    ('month', 2628000),
    ('day', 86400),
    ('hour', 3600),
    ('minute', 60),
    ('second', 1),
)

def time_string(seconds):
    items = []

    for name, count in time_intervals:
        value = seconds // count
        if value:
            seconds -= value * count
            items.append("{} {}{}".format(value, name, plural(value)))
    return ', '.join(items)

