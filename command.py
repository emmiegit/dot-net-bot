#
# command.py
#
# dot-net-bot - A Discord bot to help confused C# programmers
# Copyright (c) 2017 Ammon Smith
#
# dot-net-bot is available free of charge under the terms of the MIT
# License. You are free to redistribute and/or modify it under those
# terms. It is distributed in the hopes that it will be useful, but
# WITHOUT ANY WARRANTY. See the LICENSE file for more details.
#

from util import sanitize, time_string
import asyncio
import sys
import time
import os

__all__ = [
    'CommandExecutor',
]

class CommandExecutor(object):
    def __init__(self, state, logger):
        self.state = state
        self.logger = logger
        self.commands = {
            '!ignore': self.cmd_ignore,
            '!clear': self.cmd_clear,
            '!delete': self.cmd_delete,
            '!reload': self.cmd_reload,
            '!flush': self.cmd_flush,
            '!debug': self.cmd_debug,
            '!restart': self.cmd_restart,
            '!help': self.cmd_help,
        }

    @asyncio.coroutine
    def run(self, parts, bot, message):
        cmd = parts[0]
        args = parts[1:]
        self.logger.info("Running '{}' with {} args".format(cmd, len(args)))

        try:
            lines = self.commands[cmd](args, bot, message)
        except KeyError:
            lines = "Unknown command: {}".format(cmd)

        self.logger.info("Result from command: '{}'".format(lines))
        if not lines:
            self.logger.info("Result has no output.")
            return

        if type(lines) == str:
            self.logger.info("> {}".format(lines))
            msg = sanitize(lines)
        else:
            for line in lines:
                self.logger.info("> {}".format(line))
            msg = None

        if type(lines) in (tuple, list):
            if len(lines) < 20:
                msg = sanitize('\n'.join(lines))
            for i in range(0, len(lines), 20):
                slice = lines[i:i+20]
                self.logger.debug("Slice:")
                for line in slice:
                    self.logger.debug("> {}".format(line))

                msg = sanitize('\n'.join(slice))
                yield from bot.send_message(message.author, msg)
        elif msg:
            yield from bot.send_message(message.author, msg)

    def validate_mod(self, message):
        channel = message.channel
        member = message.author
        perms = channel.permissions_for(member)
        return perms.kick_members or perms.ban_members or perm.manage_messages

    def validate_admin(self, message):
        return message.author.id in self.state.admins

    def get_user(self, server, username):
        def lower(x):
            return x if x is None else x.lower()
        username = username.lower()
        for member in server.members:
            if username in (lower(member.nick), lower(member.name)):
                return member
        return None

    def cmd_ignore(self, args, bot, message):
        if not self.validate_mod(message):
            return "You don't have permission to do this"

        users = map(lambda x: self.get_user(message.server, x), args)
        lines = []

        for i, user in enumerate(users):
            username = args[i]

            if user is None:
                lines.append("Can't find user '{}'".format(username))
            else:
                self.state.users.add(user.id)
                lines.append("Ignoring '{}'".format(username))
            self.state.set_user_name(user.id, username)
        self.state.flush()
        return '\n'.join(lines)

    def cmd_clear(self, args, bot, message):
        if not self.validate_mod(message):
            return "You don't have permission to do this"

        users = map(lambda x: self.get_user(message.server, x), args)
        lines = []

        for i, user in enumerate(users):
            username = args[i]

            if user is None:
                lines.append("Can't find user '{}'".format(username))
            else:
                try:
                    self.state.users.remove(user.id)
                except KeyError:
                    lines.append("User '{}' wasn't on the list".format(username))
            self.state.set_user_name(user.id, username)
        self.state.flush()
        return lines

    def cmd_delete(self, args, bot, message):
        # FIXME
        return "This command is disabled."

        if not self.validate_mod(message):
            return "You don't have permission to do this"
        elif not args:
            return "Please pass in IDs in the following form '<channel id>:<message id>'"

        failed = []
        for id in args:
            try:
                cid, mid = id.split(':')
                print("_1 {}".format(id))
                channel = bot.get_channel(cid)
                print("_2 {}".format(channel))
                msg = bot.get_message(channel, mid)
                print("_2.5 {}".format(msg))
                msg = yield from bot.get_message(channel, mid)
                print("_3 {}".format(msg))
                _ = yield from bot.delete_message(msg)
            except Exception as ex:
                failed.append("Unable to delete message '{}': {}".format(id, ex))
        if failed:
            return '\n'.join(failed)
        else:
            return "Deleted all messages"

    def cmd_help(self, args, bot, message):
        return (
            "**Available commands:**",
            "[A]: requires admin permissions",
            "[M]: requires moderator permissions",
            "```",
            "!ignore  - Set the user(s) to be ignored. [M]",
            "!clear   - Clear a user's record. [M]",
            "!delete  - Deletes the given post IDs. [M]",
            "!reload  - Loads state from disk. [A]",
            "!flush   - Flush state to disk. [A]",
            "!debug   - Print some stats about the bot. [A]",
            "!restart - Restarts the bot by re-exec-ing itself. [A]",
            "!help    - Print this help message.",
            "```",
        )

    def cmd_flush(self, args, bot, message):
        if not self.validate_admin(message):
            return "You don't have permission to do this"

        self.state.flush()

    def cmd_reload(self, args, bot, message):
        if not self.validate_admin(message):
            return "You don't have permission to do this"

        self.state.reload()

    def cmd_debug(self, args, bot, message):
        if not self.validate_admin(message):
            return "You don't have permission to do this"

        self.logger.info("Generating debug info...")
        uptime = time.time() - bot.start_time

        return [
            "**Uptime:**",
            "\t" + time_string(uptime),
        ] + self.state.gen_debug_string()

    def cmd_restart(self, args, bot, message):
        if not self.validate_admin(message):
            return "You don't have permission to do this"

        self.logger.info("Restarting bot...")
        try:
            os.execv(sys.executable, ['python3'] + sys.argv)
        except OSError:
            return "Unable to re-exec bot"

