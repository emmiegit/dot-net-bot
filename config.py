#
# config.py
#
# dot-net-bot - A Discord bot to help confused C# programmers
# Copyright (c) 2017 Ammon Smith
#
# dot-net-bot is available free of charge under the terms of the MIT
# License. You are free to redistribute and/or modify it under those
# terms. It is distributed in the hopes that it will be useful, but
# WITHOUT ANY WARRANTY. See the LICENSE file for more details.
#

import json

"""
This module handles configuration, storing the default
configuration data and validating any JSON files passed
as input.
"""

__all__ = [
    'check',
    'check_file',
    'DEFAULT_CONFIG',
]

DEFAULT_CONFIG = {
    'nickname': 'C != C#',
    'game': 'with the .NET framework',
    'listen-channels': [
        'c-language',
        'dot-net-bot-test',
    ],
    'correct-channels': [
        {
            'name': 'dot-net',
            'id': '182411972409556992',
        },
        {
            'name': 'dot-net-bot-test',
            'id': '313826194732744705',
        },
    ],
    'trigger-regexes': [
        r'^c#$',
        r'[\b\s]c#[\b\s!?]',
        r'^c#\b',
        r'[\b\s]csharp[\b\s]',
        r'[\b\s]c-sharp[\b\s]',
        r'[\b\s]c sharp[\b\s]',
        r'^```cs',
        r'^```csharp',
    ],
    'admins': [
        '98633956773093376',
        '145718212552687616',
    ],
    'test-servers': [
        '98634853959876608',
    ],
    'message': \
r"""
Hello %(mention)s! You seem to be asking a question about C\#. Unfortunately this is the channel for C, not C\#.
The correct channel for C\# is actually %(dest)s.
Happy hacking!

_(Is this a mistake? You can file an issue here: <https://gitlab.com/ammongit/dot-net-bot>)_
_(Also, I have a cooldown. Don't try spamming "c#" to get me to respond. Fools.)_
""",
    'owner': 'aismallard#0175',
}

def is_string_or_null(obj):
    return type(obj) == str or \
            obj is None

def is_string_list(obj):
    if type(obj) != list:
        return False

    for item in obj:
        if type(item) != str:
            return False
    return True

def check(cfg):
    try:
        if not is_string_or_null(cfg['nickname']):
            return False
        if not is_string_or_null(cfg['game']):
            return False
        if not is_string_list(cfg['listen-channels']):
            return False
        if not is_string_list(cfg['trigger-regexes']):
            return False
        if not is_string_list(cfg['admins']):
            return False
        if not is_string_list(cfg['test-servers']):
            return False
        if type(cfg['message']) != str:
            return False
        if type(cfg['owner']) != str:
            return False
        if type(cfg['correct-channels']) != list:
            return False
        if len(cfg['correct-channels']) > 1:
            return False
    except KeyError:
        return False
    else:
        return True

def check_file(fn):
    with open(fn, 'r') as fh:
        obj = json.load(fn)
    return config_check(obj), obj

