## dot-net-bot
This is a simple [Discord](https://discordapp.com) bot that listens on `#c-language`, telling anybody who comes asking about C\# questions that it is not the correct channel, and that should go to `#dot-net` instead.
It is very configurable, so you can modify it easily to fit whatever differences you have on your server.

There are cooldowns to prevent abuse of the bot to spam a server. The bot will only post if:
* 10 minutes have passed since last post
* At least 10 posts have been made on the channel since
* The user triggering the post has never triggered the bot before

This program is licensed under the MIT License. (See `LICENSE` for details).

### Depedencies
dot-net-bot uses [discord.py](https://github.com/Rapptz/discord.py) as its backend.
```
pip3 install --user --upgrade git+https://github.com/Rapptz/discord.py@async
```
(If you want to install the requirement globally, omit the `--user` flag and run as root)

### Running
In order to run dot-net-bot, you'll need to [create a Discord application](https://discordapp.com/developers/applications/me) and get your OAuth2 id and client secret.

Credentials should be stored in `client_secret.json`, in the following form:
```json
{
    "oauth2": {
        "id": "CLIENT_ID",
        "secret": "CLIENT_SECRET"
    },
    "bot": {
        "username": "BOT_USERNAME#1000",
        "token": "BOT_TOKEN"
    }
}
```

After satisfying all the dependencies, run dot-net-bot:
```bash
python3 dot-net-bot.py [-s CLIENT_SECRET] [CONFIG_FILE]
```

Where "CONFIG FILE" is a JSON file in the following form:
```json
{
    "nickname": "DISCORD_NICKNAME_OR_NULL",
    "game": "DISCORD_GAME_OR_NULL",
    "listen-channels": [
        "c-language",
        "LIST_OF_CHANNELS"
    ],
    "correct-channel": "dot-net",
    "trigger-words": [
        "c#",
        "c-sharp",
        "c sharp",
        "ANY_OF_CASE_INSENSITIVE_KEYWORDS_YOU_WANT"
    ],
    "admins": [
        "LIST_OF_USER_IDS (not names)"
    ],
    "test-servers": [
        "LIST_OF_SERVER_IDS"
    ]
    "message": "YOUR MESSAGE HERE"
}
```

In the message, the string is %-formatted against the following dictionary:
```python
{
    'user': The username of the person who triggered the bot
    'mention': @ mention the user who triggered the bot
    'channel': Which channel they posted in
    'dest': A # mention of the channel the user meant to post to
    'trigger': Which trigger phrase they used in their post
}
```

### Adding to a server
This bot is available for public usage. Just click [here](https://discordapp.com/oauth2/authorize?&client_id=313072720474669058&scope=bot&permissions=0)

To add your own running bot to a server, open the following URL:
<tt>https://discordapp.com/oauth2/authorize?&client\_id=**(YOUR CLIENT ID HERE)**&scope=bot&permissions=0</tt>

