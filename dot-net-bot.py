#!/usr/bin/env python3

#
# dot-net-bot.py
#
# dot-net-bot - A Discord bot to help confused C# programmers
# Copyright (c) 2017 Ammon Smith
#
# dot-net-bot is available free of charge under the terms of the MIT
# License. You are free to redistribute and/or modify it under those
# terms. It is distributed in the hopes that it will be useful, but
# WITHOUT ANY WARRANTY. See the LICENSE file for more details.
#

from helper import BotHelper
from util import *
import argparse
import asyncio
import config
import discord
import logging
import sys
import time

"""
The main source in the program. This runs everything else,
including making calls to the discord.py library to initialize
the bot and post things.
"""

__all__ = [
    'LOG_FILE',
    'LOG_TO_STDOUT',
    'LOG_FILE_MODE',
]

LOG_FILE = 'bot.log'
LOG_FILE_MODE = 'w'

if __name__ == '__main__':
    # Parse arguments
    argparser = argparse.ArgumentParser(description='Run a Discord bot to help confused C# programmers')
    argparser.add_argument('-q', '--quiet', '--no-stdout',
            dest='stdout', action='store_false',
            help="Don't output to standard out.")
    argparser.add_argument('-d', '--debug',
            dest='debug', action='store_true',
            help="Set logging level to debug.")
    argparser.add_argument('-s', '--secret',
            dest='auth_file', nargs='?', default='client_secret.json',
            help="The JSON file with all the Discord authentication data. Keep it secret!")
    argparser.add_argument('-c', '--conf', '--config',
            dest='config_file', nargs='?',
            help="Specify a configuration file to use")
    args = argparser.parse_args()

    # Set up logging
    logger = logging.getLogger('discord')
    if args.debug:
        logger.setLevel(level=logging.DEBUG)
    else:
        logger.setLevel(level=logging.INFO)

    log_fmtr = logging.Formatter("%(asctime)s [%(levelname)s] %(name)s: %(message)s",
        datefmt="[%d/%m/%Y %H:%M]")

    log_handler = logging.FileHandler(filename=LOG_FILE, encoding='utf-8', mode=LOG_FILE_MODE)
    log_handler.setFormatter(log_fmtr)
    logger.addHandler(log_handler)

    if args.stdout:
        log_handler = logging.StreamHandler(sys.stdout)
        log_handler.setFormatter(log_fmtr)
        logger.addHandler(log_handler)

    # Get and verify configuration
    if args.config_file is None:
        logger.info("No configuration file passed. Using default...")
        cfg = config.DEFAULT_CONFIG
    else:
        cfg, valid = config.check_file(args.config_file)
        if not valid:
            print("Configuration file is invalid. Review README.md for what it should look like.")

    # Open client
    bot = discord.Client()
    bot.logger = logger
    bot.start_time = time.time()
    helper = BotHelper(cfg, logger)

    # Create event handlers
    @bot.async_event
    def on_ready():
        # Print welcome string
        users = len(set(bot.get_all_members()))
        servers = len(bot.servers)
        channels = len([c for c in bot.get_all_channels()])
        logger.info("")
        logger.info("{} is now online.".format(bot.user.name))
        logger.info("")
        logger.info("Connected to:")
        logger.info(" * {} server{}".format(servers, plural(servers)))
        logger.info(" * {} channel{}".format(channels, plural(channels)))
        logger.info(" * {!r} user{}".format(users, plural(users)))
        logger.info("")

        # Set default settings
        if cfg['game']:
            yield from bot.change_presence(game=discord.Game(name=cfg['game']))
        for server in bot.servers:
            nickname = cfg['nickname']
            logger.info("Changing nick to '{}'".format(nickname))
            try:
                logger.debug("Changing nickname on '{}'".format(server.name))
                yield from bot.change_nickname(server.me, nickname)
            except:
                logger.info("Unable to change nickname on '{}'".format(server.name))

        logger.info("Listening on:")
        for channel in cfg['listen-channels']:
            logger.info("* {}".format(channel))

        # All done setting up
        logger.info("Ready!")

    @bot.async_event
    def on_message(message):
        logger.debug("Received message {}".format(message.id))

        if message.channel.is_private and message.author != bot.user:
            logger.info("Got a PM from {}".format(message.author))
            yield from bot.send_message(message.channel, "I don't respond to PMs\nContact {} for help".format(cfg['owner']))
            return

        if message.author == bot.user or message.channel.name not in helper.channels:
            logger.debug("Ignoring message.")
            return

        logger.debug("Incrementing message count")
        helper.state.incr_channel(message.channel)
        helper.state.set_channel_name(message.channel)

        logger.debug("Got message from {}#{}: '{}'".format(
            message.server.name, message.channel.name, message.content))
        ran = yield from helper.run_command(bot, message)
        if ran:
            return

        match = helper.matches(message.content)
        if match:
            logger.info("Found post on {} with trigger phrase: '{}'".format(message.server.id, message.content))
            if helper.timeout.ok_to_post(message):
                logger.info("OK to post. Sending response...")
                response = helper.format({
                    'user': message.author.name,
                    'mention': message.author.mention,
                    'channel': message.channel.name,
                    'dest': helper.get_correct_channel_mention(message.server),
                    'trigger': match,
                })
                yield from bot.send_message(message.channel, sanitize(response))
            else:
                logger.info("Posted too recently, no response will be sent")
        else:
            logger.debug("Post doesn't match, no response will be sent")

    # Get the authentication token
    client_secret = get_client_secret(args.auth_file)
    token = client_secret['bot']['token']
    del client_secret

    # Run the bot
    bot.run(token)

