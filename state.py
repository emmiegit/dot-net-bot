#
# state.py
#
# dot-net-bot - A Discord bot to help confused C# programmers
# Copyright (c) 2017 Ammon Smith
#
# dot-net-bot is available free of charge under the terms of the MIT
# License. You are free to redistribute and/or modify it under those
# terms. It is distributed in the hopes that it will be useful, but
# WITHOUT ANY WARRANTY. See the LICENSE file for more details.
#

from __future__ import with_statement
import json
import os
import time

__all__ = [
    'State',
]

class State(object):
    def __init__(self, admins, test_servers, logger, filename='state.json'):
        self.logger = logger
        self.state_fn = filename
        self.backup_fn = filename + '.bak'
        self.admins = admins
        self.test_servers = test_servers

        self.names = {
            'servers': {},
            'channels': {},
            'users': {},
        }
        self.servers = {} # server_id: last_time
        self.channels = {} # channel_id: posts
        self.users = set() # user_id
        self.reload()

    # General
    def get_message_stats(self, message):
        sid = message.server.id
        cid = message.channel.id
        uid = message.author.id

        last = self.servers.get(sid, 0)
        posts = self.channels.get(cid, -1)
        posted = uid in self.users

        return (last, posts, posted)

    def set_message_stats(self, message, now):
        sid = message.server.id
        cid = message.channel.id
        uid = message.author.id

        self.servers[sid] = now
        self.reset_channel(message.channel)
        self.users.add(uid)

    def incr_channel(self, channel):
        cid = channel.id
        self.channels[cid] = self.channels.get(cid, 0) + 1

    def reset_channel(self, channel):
        cid = channel.id
        self.channels[cid] = 0

    # Name cache
    def get_server_name(self, sid):
        return self.names['servers'].get(sid, "<unknown>")

    def set_server_name(self, server):
        sid = server.id
        name = server.name
        self.names['servers'][sid] = name

    def get_channel_name(self, cid):
        return self.names['channels'].get(cid, "<unknown>")

    def set_channel_name(self, channel):
        cid = channel.id
        name = channel.name
        self.names['channels'][cid] = name

    def get_user_name(self, uid):
        return self.names['users'].get(uid, "<unknown>")

    def set_user_name(self, user):
        uid = user.id
        name = user.name
        self.names['users'][uid] = name

    # Misc
    def gen_debug_string(self):
        lines = []

        lines.append("**Server timeouts:**")
        if not self.servers:
            lines.append("\t(none)")
        for server, last in self.servers.items():
            lines.append("\t{} - {}".format(self.get_server_name(server), time.ctime(last)))

        lines.append("**Channel posts since:**")
        if not self.channels:
            lines.append("\t(none)")
        for channel, posts in self.channels.items():
            lines.append("\t#{} - {}".format(self.get_channel_name(channel), posts))

        lines.append("**User triggered:**")
        if not self.users:
            lines.append("\t(none)")
        for user in self.users:
            lines.append("\t{}".format(self.get_user_name(user)))

        lines.append("**Raw:**")
        lines.append("\tnames: `{!r}`".format(self.names))
        lines.append("\tservers: `{!r}`".format(self.servers))
        lines.append("\tchannels: `{!r}`".format(self.channels))
        lines.append("\tusers: `{!r}`".format(self.users))
        lines.append("\tadmins: `{!r}`".format(self.admins))
        lines.append("\ttest-servers: `{!r}`".format(self.test_servers))
        return lines

    # Serial
    def flush(self):
        self.logger.info("Flushing state to disk...")
        if os.path.exists(self.backup_fn):
            try:
                os.unlink(self.backup_fn)
            except OSError:
                pass

        if os.path.exists(self.state_fn):
            try:
                os.rename(self.state_fn, self.backup_fn)
            except OSError as err:
                self.logger.error("Unable to rename dump file: '{}' -> '{}': {}".format(
                    self.state_fn, self.backup_fn, err))
                return False
        try:
            with open(self.state_fn, 'w') as fh:
                json.dump({
                    'names': self.names,
                    'servers': self.servers,
                    'channels': self.channels,
                    'users': list(self.users),
                }, fh)
        except Exception as ex:
            self.logger.error("Unable to dump timeout state to JSON: {!s}".format(ex))
            return False
        else:
            return True

    def reload(self):
        self.logger.info("Loading state from disk...")
        filenames = (
            self.state_fn,
            self.backup_fn,
        )
        for filename in filenames:
            obj = self._load(filename)
            if obj:
                break
        if obj:
            self.names = obj['names']
            self.servers = obj['servers']
            self.channels = obj['channels']
            self.users = set(obj['users'])
        return bool(obj)

    def _load(self, fn):
        if os.path.exists(fn):
            self.logger.debug("'{}' exists, reading...".format(fn))
            try:
                with open(fn, 'r') as fh:
                    return json.load(fh)
            except Exception as ex:
                self.logger.error("Unable to load timeout state from JSON: {!s}".format(ex))
        return None

