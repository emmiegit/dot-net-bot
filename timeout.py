#
# timeout.py
#
# dot-net-bot - A Discord bot to help confused C# programmers
# Copyright (c) 2017 Ammon Smith
#
# dot-net-bot is available free of charge under the terms of the MIT
# License. You are free to redistribute and/or modify it under those
# terms. It is distributed in the hopes that it will be useful, but
# WITHOUT ANY WARRANTY. See the LICENSE file for more details.
#

from __future__ import with_statement
import json
import time
import os

__all__ = [
    'TIMEOUT_GLOBAL',
    'POSTS_UNTIL_NOTIFY',
    'Timeout',
]

TIMEOUT_GLOBAL = 10 * 60 # in seconds
POSTS_UNTIL_NOTIFY = 10

class Timeout(object):
    def __init__(self, state, logger):
        self.state = state
        self.logger = logger

    def ok_to_post(self, message):
        last, posts, posted = self.state.get_message_stats(message)
        now = int(time.time())

        # Test servers are always OK
        if message.server.id in self.state.test_servers:
            return True

        # Check user
        if posted:
            user = self.state.get_user_name(message.author.id)
            self.logger.info("User {} has already posted".format(user))
            return False

        # Check channel
        if 0 <= posts < POSTS_UNTIL_NOTIFY:
            channel = self.state.get_channel_name(message.channel.id)
            self.logger.info("Only {} posts have been made on {} (wants {})".format(
                posts, channel, POSTS_UNTIL_NOTIFY))
            return False

        # Check server
        self.logger.debug("Current time is {}".format(now))
        diff = now - last
        if diff < TIMEOUT_GLOBAL:
            server = self.state.get_server_name(message.server.id)
            self.logger.info("Server global timeout has not elapsed (last: {}, diff: {})".format(last, diff))
            return False

        # OK to post, update state
        self.state.set_message_stats(message, now)
        self.state.set_user_name(message.author)
        self.state.set_channel_name(message.channel)
        self.state.set_server_name(message.server)
        self.state.flush()
        return True

